<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Himatika - Unuja</title>

 	  <style>
      body {
          margin: 0;
          padding: 0;
          font-family: "Roboto", Helvetica, sans-serif;
          color: #0E456B;
          font-size: 12pt;
          font-weight: 300;
          line-height: 1.75em;
      }

      .error-page {
        margin: auto;
      }

      .text-center {
        text-align: center;
      }

      h1 {
        color: #A51010;
        font-weight: bold;
        text-transform: uppercase;
      }
    </style>
  </head>

  <body>
    <div class="container" style="margin-top: 200px;">
        <div class="error-page">
            <div class="text-center">
              <h1>Error 403</h1>
              <h2>Wow !!! Hacking Detected</h2>
            </div>
        </div>
    </div>

  </body>
</html>