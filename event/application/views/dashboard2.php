<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
 ?>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        {_meta}
        <title>{title}</title>
        <!--[if IE]> 
        <script> (function() { var html5 = ("abbr,article,aside,audio,canvas,datalist,details," + "figure,footer,header,hgroup,mark,menu,meter,nav,output," + "progress,section,time,video").split(','); for (var i = 0; i < html5.length; i++) { document.createElement(html5[i]); } try { document.execCommand('BackgroundImageCache', false, true); } catch(e) {} })(); </script> <![endif]-->
		<!-- template style -->
        
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/skel.css">

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
        <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/skel.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/skel-layers.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/init.js"></script>
        
        {_styles}
        {_scripts}
    </head>
    <body>
   


        <!-- Header -->
            <header id="header" class="skel-layers-fixed">
               
                <h1><a href="intro">Himatika Event 2018</a></h1>
                <nav id="nav">
                    <ul>
                        <li><a href="list_peserta" >Peserta</a></li>
                        <li><a href="list_pembayaran">Pembayaran</a></li>
                        <li><a href="logout" >Logout</a></li>
                    </ul>

                </nav>
            </header>
            
        {content}
        <!-- Footer -->
        <footer id="footer">
            <ul class="copyright">
                <li>&copy; 2018 Himatika</li>
            </ul>
        </footer>
    </body>
</html>