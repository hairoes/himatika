

<!-- Main -->
			<section id="main" class="wrapper style1">
				
<header class="major">
					<h2>SYARAT & KETENTUAN</h2>
					<p>Harap dibaca terlebih dahulu</p>
				</header>
			

				<div class="container">
						<div class="12u">
							
							<section class="cd-faq">
	<ul class="cd-faq-categories alt">
		<li><a class="selected" href="#basics">Tentang</a></li>
		<li><a href="#mobile">Pelaksanaan</a></li>
		<li><a href="#payments">Pendaftaran</a></li>
		<li><a href="#privacy">Penilaian</a></li>
		<li><a href="#delivery">Hadiah</a></li>
	</ul> <!-- cd-faq-categories -->

	<div class="cd-faq-items">
		<ul id="basics" class="cd-faq-group alt">
			<li class="cd-faq-title"><h2>Tentang</h2></li>
			<li>
				<a class="cd-faq-trigger" href="#0">Web Design Competition</a>
				<div class="cd-faq-content">

					<ul>
						<li>Web Design Competition adalah salah satu kegiatan lomba yang dimiliki oleh Himpunan Mahasiswa Program Studi Informatika Fakultas Teknik Universitas Nurul Jadid. Kegiatan ini ditujukan kepada siswa-siswi SMK Se - JATIM. Melalui kegiatan ini, diharapkan agar muncul pemuda pemudi yang berkualitas dan berkreatifitas serta mempunyai kemampuan dalam dunia pendidikan yang berbasis teknologi maupun dalam bidang desain website sebagai bekal untuk menghadapi era globalisasi di masa yang akan mendatang dengan bekal pendidikan yang basisnya teknologi.  </li>
						<li>Dalam rangka memudahkan peserta lomba untuk memahami syarat dan ketentuan yang berlaku dalam Kegiatan Lomba Web Design Competition UNUJA 2018, berikut kami sertakan panduan lomba sebagai referensi bagi para peserta lomba yang akan mengikuti kegiatan ini.</li>
					</ul>
				</div> <!-- cd-faq-content -->
			</li>
			<li>
				<a class="cd-faq-trigger" href="#0">Syarat & Ketentuan Peserta</a>
				<div class="cd-faq-content">
					<ol>
						<li>Peserta adalah siswa SMK se-JATIM.</li>
						<li>Peserta lomba adalah perorangan.</li>
						<li>Setiap sekolah yang berpartisipasi hanya dapat mengirimkan maksimal 3 orang perwakilannya.</li>
						<li>Setiap sekolah hanya boleh didampingi oleh 1 (satu) orang pembina.</li>
					</ol>
				</div> <!-- cd-faq-content -->
			</li>
			
		</ul> <!-- cd-faq-group -->

		<ul id="mobile" class="cd-faq-group alt">
			<li class="cd-faq-title"><h2>Pelaksanaan</h2></li>
			<li>
				<a class="cd-faq-trigger" href="#0">Penyisihan & Final</a>
				<div class="cd-faq-content">
					<ol>
						<li>Hari Rabu s/d Kamis</li>
						<li>Tanggal 25 s/d 26 April 2018</li>
						<li>Pukul 08.00 - Selesai</li>
						<li>Tempat Aula Fakultas Teknik Universitas Nurul Jadid</li>
					</ol>
				</div> <!-- cd-faq-content -->
			</li>
			<li>
				<a class="cd-faq-trigger" href="#0">Teknis Pelaksaan</a>
				<div class="cd-faq-content">
					<ol>
						<li>Tema lomba yang ditetapkan untuk kegiatan Web Design Competition adalah <b>“Create The Future With Technology”</b>.  </li>
						<li>2.Peserta merupakan pelajar SMK se-Jatim dan masih berstatus siswa/i aktif (belum lulus).</li>
						<li>Peserta wajib melunasi biaya pendaftaran sebesar Rp. 75.000,00.</li>
						<li>Pelaksanaan lomba terdiri dari 3 (tiga) babak yaitu :</li>
						<ul>
							<li>Babak penyisihan, Pada babak ini peserta diwajibkan untuk mendesain sebuah website sesuai dengan tema lomba yang telah ditetapkan oleh panitia.  </li>
							<li>Babak Semi Final (pengembangan), pada babak ini hanya diambil 20 peserta terbaik, dan peserta akan mengembangkan hasil karya yang pertama dengan menambah database.</li>
							<li>Babak Final. Pada babak ini akan di ambil 10 orang untuk mempresentasikan hasil karyanya. Waktu yang diberikan kepada masing-masing Finalis untuk mempresentasikan hasil karyanya yaitu sebanyak 10 menit yang disertai dengan tanya jawab dengan dewan juri yaitu sebanyak 15 menit.</li>
						</ul>
						<li>Semua perlengkapan seperti Laptop, Software dan lain lain. Disediakan sendiri oleh masing-masing peserta.</li>
						<li>Tidak boleh memakai template website</li>
						<li>Dilarang menggunakan <i>framework css (bootstrap, foundation, less Framework, amazium, skeleton, dll.)</i> maupun template dari orang lain.</li>
						<li>Responsive.</li>
						<li>Hasil karya tidak boleh bertentangan dengan isu <b>SARA</b> dan norma hukum yang berlaku.</li>
						<li>Hasil karya yang dibuat adalah hasil kreaativitas sendiri, belum pernah dipublikasikan kepada pihak manapun, dan belum pernah di ikutsertakan dalam lomba lainnya yang sejenis,jika peserta melakukan <strong>PLAGIAT</strong> maka peserta akan di diskualifikasi oleh panitia lomba .</li>
						<li>Waktu yang diberikan pada saat pelaksanaan lomba untuk mengerjakan desain web maksimal 180 menit.</li>
						<li>Bahan-bahan lomba berupa teks, gambar, logo, ikon, jquery, font, video serta bahan-bahan lainnya yang diperlukan disediakan sendiri oleh peserta pada saat pelaksanaan lomba.</li>
						<li>Pemenang Lomba Web Design Competition ditentukan melalui Sistem Juri.</li>
						<li>Keputusan dewan juri adalah mutlak dan tidak dapat diganggu gugat.</li>
						<li>Hasil karya akhirnya akan tetap menjadi hak milik peserta, namun panitia juga memiliki hak atas karya peserta sebatas arsip panitia (panitia tidak akan mempublikasikan ataupun menjual hasil karya peserta).</li>
					</ol>
					
				</div> <!-- cd-faq-content -->
			</li>

		</ul>
			

		<ul id="payments" class="cd-faq-group alt">
			<li class="cd-faq-title"><h2>Pendaftaran</h2></li>
			<li>
				<a class="cd-faq-trigger" href="#0">Online</a>
				<div class="cd-faq-content">
					<ol>
						<li>Peserta melakukan pendaftaran dengan mengakses link <a href="login">Disini</a>.</li>
						<li>Setelah pendaftaran selesai, peserta harus melakukan pembayaran biaya pendaftaran sebesar Rp. 75.000,- melalui transfer  ke Nomor Rekening (748-0010-0406-1538 BRI ) a.n. Hendra Firmansyah.</li>
						<li>Setelah proses transfer dilakukan, peserta harus mengirim foto bukti transfer beserta softcopy  kartu pelajar ke alamat <a href="konfirmasi">Disini</a> dalam waktu paling lambat 3x24 jam.</li>
					</ol>
				</div> <!-- cd-faq-content -->
			</li>
			
			<li>
				<a class="cd-faq-trigger" href="#0">Offline</a>
				<div class="cd-faq-content">
					<ol>
						<li>Peserta menghubungi panitia a.n <strong>Hendra Firmansyah</strong> (082230904397) dan melakukan pertemuan secara langsung dikantor Himpunan Mahasiswa Program Studi Informatika (HIMATIKA) Fakultas Teknik  Universitas Nurul Jadid pada waktu pendaftaran yang telah ditentukan.</li>
						<li>Peserta mengisi formulir pendaftaran, fotocopy kartu pelajar beserta uang pendaftaran sebesar Rp. 75.0000,- pada saat pertemuan.</li>
						
					</ol>
					<p>NB : panitian tidak bertanggung jawab apabila terjadi kesalahan yang dilakukan oleh peserta dalam melakukan pembayaran melalui nomor rekening</p>
				</div> <!-- cd-faq-content -->
			</li>

			
		</ul> <!-- cd-faq-group -->

		<ul id="privacy" class="cd-faq-group alt">
			<li class="cd-faq-title"><h2>Penilaian</h2></li>
			<li>
				<a class="cd-faq-trigger" href="#0">Kriteria Penilaian Yang Belaku</a>
				<div class="cd-faq-content">
					<h4>Babak Penyisihan</h4>
					<ol>
						<li>Interface dan design :  60%  </li>
						<li>Kreativitas dan inovasi 40%  </li>
					</ol>
					<h4>Babak Semi Final</h4>
					<ol>
						<li>Kreativitas dan inovasi 30%</li>
						<li>Kecepatan akses 70%</li>
					</ol>
					<h4>Babak Final</h4>
					<ol>
						<li>Presentasi : 60%  </li>
						<li>Tanya jawab : 40% </li>
					</ol>
					
				</div> <!-- cd-faq-content -->
			</li>

			
		</ul> <!-- cd-faq-group -->

		<ul id="delivery" class="cd-faq-group alt">
			<li class="cd-faq-title"><h2>Hadiah</h2></li>
			<li>
				<a class="cd-faq-trigger" href="#0">Hadiah Yang Bisa Didapatkan</a>
				<div class="cd-faq-content">
					<h4>Juara Pertama</h4>
					<ol>
						<li>Uang Tunai Rp. 1.500.000,-</li>
						<li>Trophy + Sertifikat </li>
					</ol>
					<h4>Juara Kedua</h4>
					<ol>
						<li>Uang Tunai Rp. 1.000.000,-</li>
						<li>Trophy + Sertifikat </li>
					</ol>

					<h4>Juara Ketiga</h4>
					<ol>
						<li>Uang Tunai Rp. 750.000,-</li>
						<li>Trophy + Sertifikat </li>
					</ol>
					<h4>Juara Harapan I</h4>
					<ol>
						<li>Trophy + Sertifikat </li>
					</ol>
					<h4>Juara Harapan II</h4>
					<ol>
						<li>Trophy + Sertifikat </li>
					</ol>
					<h4>Harapan III</h4>
					<ol>
						<li>Trophy + Sertifikat </li>
					</ol>
				</div> <!-- cd-faq-content -->
			</li>

			
		</ul>
		
	</div> <!-- cd-faq-items -->
	<a href="#0" class="cd-close-panel">Close</a>
</section> <!-- cd-faq -->


						</div>
					
				</div>
			</section>

