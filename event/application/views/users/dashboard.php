<?php 
defined('BASEPATH') OR exit('No direct script access allowed');
 ?>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        {_meta}
        <title>Event Himatika 2018</title>

        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/skel.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/style.css">
        <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/skel.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/skel-layers.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/init.js"></script>
        {_styles}
        {_scripts}
    </head>
    <body>
        <header id="header" class="skel-layers-fixed">

            <h1><a href="#">Himatika Event 2018</a></h1>
            <nav id="nav">
                <ul>
                    <li><a href="home" >Home</a></li>
                    <li><a href="profil" >Profil</a></li>
                    <li><a href="konfirmasi" >Pembayaran</a></li>
                    <li><a href="susunan_acara" >Susunan Acara</a></li>
                    <li><a href="logout" >Logout</a></li>
                </ul>

            </nav>
        </header>
            
        {content}
        <!-- Footer -->
        <footer id="footer">
            <ul class="copyright">
                <li>&copy; 2018 Himatika</li>
            </ul>
        </footer>
    </body>
</html>