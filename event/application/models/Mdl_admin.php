<?php 
/**
* 
*/
class Mdl_admin extends CI_Model
{
	protected $table = 'admin';
	function __construct()
	{
		$this->load->database();
	}
	/**
  	 * [delete_user from user table use id_user]
  	 * @param  [sting] $id [id parameter]
  	 * @return [boolean]     [true]
  	 */
  	public function delete_user($id)
  	{
  		$this->db->delete()
  			->from('user')
  			->where('id_user', $id);

 		return TRUE;
  	}
	  
	public function login($data = array())
  	{
	    $this->db->from('admin');
	    $this->db->where($data);
	    return $this->db->get()->row();
  	}
  	

  	public function get_peserta()
  	{
  		$this->db->from('user');
	    $this->db->order_by('nama_sekolah', 'asc');
	    return $this->db->get()->result();
  	}

  	public function get_detail_peserta($id = '')
  	{
  		$this->db->select('*');
	    $this->db->from('user'); 
	    $this->db->where('id_user', $id);       
	    $query = $this->db->get(); 
	    if($query->num_rows() != 0)
	    {
	        return $query->row();
	    }
	    else
	    {
	        return false;
	    }
  	}

  	public function get_pembayaran($status = '')
  	{
  		$this->db->select('t1.*,t2.*')
	  		->from('pembayaran as t1') 
	  		->where('t1.status', $status)
		    ->join('user as t2', 't1.id_user=t2.id_user', 'LEFT')
	    	->order_by('t2.nama','asc'); 
	    $query = $this->db->get(); 
	    if($query->num_rows() != 0)
	    {
	        return $query->result();
	    }
	    else
	    {
	        return false;
	    }

  	}

  	
  	public function get_img($value='')
  	{
  		$this->db->select('foto');
  		$this->db->from('pembayaran');
  		$this->db->where('id_user', $value);
		return $this->db->get()->row();
  	}

  	public function approve_pembayaran($id)
  	{
  		return $this->db->set('status', 'y')
			->where('id_user', $id)
			->update('pembayaran');
  	}

  	public function approve_peserta($id)
  	{
  		return $this->db->set('status', 'y')
			->where('id_user', $id)
			->update('user');
  	}

  	public function get_admin($value='')
  	{
	    $this->db->from('admin');
	    $this->db->where('id_admin', $value);
	    return $this->db->get()->row();
  	}
}